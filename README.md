# Recipe App API Proxy
This is the Nginx proxy app for the recipe app api

## Usage

### Environment Variables
" * 'LISTEN_PORT' - Port to listen on (default: '8000') "
" * 'APP_HOST' - Hostname of the app to forward requests to (default: 'app')"
" * 'APP_PORT' - Port of the app to forward the requests to (default: '9000')"

